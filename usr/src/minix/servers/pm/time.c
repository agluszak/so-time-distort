/* This file takes care of those system calls that deal with time.
 *
 * The entry points into this file are
 *   do_getres:		    perform the CLOCK_GETRES system call
 *   do_gettime:	    perform the CLOCK_GETTIME system call
 *   do_settime:	    perform the CLOCK_SETTIME system call
 *   do_time:		    perform the GETTIMEOFDAY system call
 *   do_stime:		    perform the STIME system call
 *   do_distort_time    perform the DISTORT_TIME system call
 */

#include "pm.h"
#include <minix/callnr.h>
#include <minix/com.h>
#include <signal.h>
#include <sys/time.h>
#include <stdio.h>
#include "mproc.h"

#define NSECS_IN_SEC 1000000000

/*===========================================================================*
 *				do_gettime				     *
 *===========================================================================*/
int do_gettime() {
    clock_t ticks, realtime, clock;
    time_t boottime;
    int s;

    if ((s = getuptime(&ticks, &realtime, &boottime)) != OK)
        panic("do_time couldn't get uptime: %d", s);

    switch (m_in.m_lc_pm_time.clk_id) {
        case CLOCK_REALTIME:
            clock = realtime;
            break;
        case CLOCK_MONOTONIC:
            clock = ticks;
            break;
        default:
            return EINVAL; /* invalid/unsupported clock_id */
    }

    mp->mp_reply.m_pm_lc_time.sec = boottime + (clock / system_hz);
    mp->mp_reply.m_pm_lc_time.nsec =
            (uint32_t)((clock % system_hz) * 1000000000ULL / system_hz);

    return (OK);
}

/*===========================================================================*
 *				do_getres				     *
 *===========================================================================*/
int do_getres() {
    switch (m_in.m_lc_pm_time.clk_id) {
        case CLOCK_REALTIME:
        case CLOCK_MONOTONIC:
            /* tv_sec is always 0 since system_hz is an int */
            mp->mp_reply.m_pm_lc_time.sec = 0;
            mp->mp_reply.m_pm_lc_time.nsec = 1000000000 / system_hz;
            return (OK);
        default:
            return EINVAL; /* invalid/unsupported clock_id */
    }
}

/*===========================================================================*
 *				do_settime				     *
 *===========================================================================*/
int do_settime() {
    int s;

    if (mp->mp_effuid != SUPER_USER) {
        return (EPERM);
    }

    for ( struct mproc* rmp = &mproc[0]; rmp < &mproc[NR_PROCS]; rmp++)
        rmp->reference_set = 0;

    switch (m_in.m_lc_pm_time.clk_id) {
        case CLOCK_REALTIME:
            s = sys_settime(m_in.m_lc_pm_time.now, m_in.m_lc_pm_time.clk_id,
                            m_in.m_lc_pm_time.sec, m_in.m_lc_pm_time.nsec);
            return (s);
        case CLOCK_MONOTONIC: /* monotonic cannot be changed */
        default:
            return EINVAL; /* invalid/unsupported clock_id */
    }
}

/*===========================================================================*
 *				do_time					     *
 *===========================================================================*/
int do_time() {
/* Perform the time(tp) system call. This returns the time in seconds since 
 * 1.1.1970.  MINIX is an astrophysically naive system that assumes the earth 
 * rotates at a constant rate and that such things as leap seconds do not 
 * exist.
 */
    clock_t ticks, realtime;
    time_t boottime;
    int s;

    if ((s = getuptime(&ticks, &realtime, &boottime)) != OK)
        panic("do_time couldn't get uptime: %d", s);

    time_t sec = boottime + (realtime / system_hz);
    long nsec = (uint32_t)((realtime % system_hz) * 1000000000ULL / system_hz);

    mp->mp_reply.m_pm_lc_time.sec = sec;
    mp->mp_reply.m_pm_lc_time.nsec = nsec;

    if (mp->was_distorted == 0) {
        return (OK);
    }

    if (mp->reference_set == 0) {
        mp->distortion_reference_sec = sec;
        mp->distortion_reference_nsec = nsec;
        mp->reference_set = 1;
        return (OK);
    }


    time_t d_sec = sec - mp->distortion_reference_sec;
    long d_nsec = nsec - mp->distortion_reference_nsec;
    if (d_nsec < 0) {
        d_sec -= 1;
        d_nsec += NSECS_IN_SEC;
    }

    if (mp->distortion_scale == 0) {
        mp->mp_reply.m_pm_lc_time.sec = mp->distortion_reference_sec;
        mp->mp_reply.m_pm_lc_time.nsec = mp->distortion_reference_nsec;
        return (OK);
    }

    long long d_nsec_total = (long long) d_nsec + (long long) d_sec * NSECS_IN_SEC;

    if (mp->distortion_scale < 0) {
        d_nsec_total /= (-mp->distortion_scale);
    } else {
        d_nsec_total *= mp->distortion_scale;
    }

    time_t new_sec = mp->distortion_reference_sec + (d_nsec_total / NSECS_IN_SEC);
    long new_nsec = (mp->distortion_reference_nsec + d_nsec_total) % NSECS_IN_SEC;
    mp->mp_reply.m_pm_lc_time.sec = new_sec;
    mp->mp_reply.m_pm_lc_time.nsec = new_nsec;

    return (OK);

}

/*===========================================================================*
 *				do_stime				     *
 *===========================================================================*/
int do_stime() {
/* Perform the stime(tp) system call. Retrieve the system's uptime (ticks 
 * since boot) and pass the new time in seconds at system boot to the kernel.
 */
    clock_t uptime, realtime;
    time_t boottime;
    int s;

    if (mp->mp_effuid != SUPER_USER) {
        return (EPERM);
    }
    if ((s = getuptime(&uptime, &realtime, &boottime)) != OK)
        panic("do_stime couldn't get uptime: %d", s);
    boottime = m_in.m_lc_pm_time.sec - (realtime / system_hz);

    s = sys_stime(boottime);        /* Tell kernel about boottime */
    if (s != OK)
        panic("pm: sys_stime failed: %d", s);

    return (OK);
}

// 1 if C is a ancestor of P
// 0 if no relationship
// -1 if C is a descendant of P
int find_relationship(struct mproc* c_ptr, struct mproc* p_ptr) {
    int c_index = (c_ptr - &mproc[0]);
    int p_index = (p_ptr - &mproc[0]);

    int i = p_index;
    while (mproc[i].mp_pid != 1) {
        i = mproc[i].mp_parent;
        if (i == c_index) {
            return 1;
        }
    }

    i = c_index;
    while (mproc[i].mp_pid != 1) {
        i = mproc[i].mp_parent;
        if (i == p_index) {
            return -1;
        }
    }

    return 0;
}

/*===========================================================================*
 *				do_distort_time				     *
 *===========================================================================*/
int do_distort_time() {
    pid_t c_pid = mp->mp_pid;
    pid_t p_pid = m_in.m_pm_distort_time.distorted_pid;

    if (c_pid == p_pid ) {
        return EPERM;
    }

    struct mproc* c_ptr = mp;
    struct mproc* p_ptr = find_proc(p_pid);

    if (p_ptr == NULL) {
        return EINVAL;
    }

    int relationship = find_relationship(c_ptr, p_ptr);
    if (relationship == 0) {
        return EPERM;
    }

    uint8_t scale = m_in.m_pm_distort_time.scale;
    p_ptr->was_distorted = 1;
    p_ptr->distortion_scale = scale * relationship;

    return (OK);
}