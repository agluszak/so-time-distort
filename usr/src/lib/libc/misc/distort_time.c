/* execve() - basic program execution call */

#include <sys/cdefs.h>
#include "namespace.h"
#include <lib.h>

#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <minix/param.h>
#include <sys/exec_elf.h>
#include <sys/exec.h>

int distort_time(pid_t pid, uint8_t scale) {
    message m;

    /* Clear unused message fields */
    memset(&m, 0, sizeof(m));
    m.m_pm_distort_time.distorted_pid = pid;
    m.m_pm_distort_time.scale = scale;

    int return_value = _syscall(PM_PROC_NR, PM_DISTORT_TIME, &m);
    if (return_value < 0) {
        return errno;
    }

    return 0;
}
